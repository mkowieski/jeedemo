package com.example.jeedemo.service;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.example.jeedemo.domain.Specialization;
import com.example.jeedemo.domain.Worker;


/* 
 * This is a Stateless EJB Bean
 * All its methods are transactional
 */
@Stateless
public class SpecManager {

	@PersistenceContext
	EntityManager em;

	public void goSpec(Long workerId, Long specId) {

		Worker worker = em.find(Worker.class, workerId);
		Specialization specialization = em.find(Specialization.class, specId);
		specialization.setSpec(true);

		worker.getSpecializations().add(specialization);
	}

	@SuppressWarnings("unchecked")
	public List<Specialization> getAvailableSpecializations() {
		return em.createNamedQuery("specialization.unspec").getResultList();
	}

	public void disposeSpecialization(Worker worker, Specialization specialization) {

		worker = em.find(Worker.class, worker.getId());
		specialization = em.find(Specialization.class, specialization.getId());

		Specialization toRemove = null;
		// lazy loading here (person.getCars)
		for (Specialization aSpecialization : worker.getSpecializations())
			if (aSpecialization.getId().compareTo(specialization.getId()) == 0) {
				toRemove = aSpecialization;
				break;
			}

		if (toRemove != null)
			worker.getSpecializations().remove(toRemove);
		
		specialization.setSpec(false);
	}
}
