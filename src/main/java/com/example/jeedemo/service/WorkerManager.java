package com.example.jeedemo.service;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.example.jeedemo.domain.Specialization;
import com.example.jeedemo.domain.Worker;

@Stateless
public class WorkerManager {

	@PersistenceContext
	EntityManager em;

	public void addWorker(Worker worker) {
		worker.setId(null);
		em.persist(worker);
	}

	public void deleteWorker(Worker worker) {
		worker = em.find(Worker.class, worker.getId());
		em.remove(worker);
	}

	@SuppressWarnings("unchecked")
	public List<Worker> getAllWorkers() {
		return em.createNamedQuery("worker.all").getResultList();
	}

	public List<Specialization> getOwnedSpecializations(Worker worker) {
		worker = em.find(Worker.class, worker.getId());
		// lazy loading here - try this code without this (shallow) copying
		List<Specialization> specializations = new ArrayList<Specialization>(worker.getSpecializations());
		return specializations;
	}
}
