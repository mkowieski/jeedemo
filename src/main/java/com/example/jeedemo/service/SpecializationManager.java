package com.example.jeedemo.service;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.example.jeedemo.domain.Specialization;

@Stateless
public class SpecializationManager {

	@PersistenceContext
	EntityManager em;

	public void addSpecialization(Specialization specialization) {
		specialization.setId(null);
		em.persist(specialization);
	}

	public void deleteSpecialization(Specialization specialization) {
		specialization = em.find(Specialization.class, specialization.getId());
		em.remove(specialization);
	}

	@SuppressWarnings("unchecked")
	public List<Specialization> getAllSpecializations() {
		return em.createNamedQuery("specialization.all").getResultList();
	}
}
