package com.example.jeedemo.service;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.example.jeedemo.domain.Device;

@Stateless
public class DeviceManager {

	@PersistenceContext
	EntityManager em;

	public void addDevice(Device device) {
		device.setId(null);
		em.persist(device);
	}

	public void deleteDevice(Device device) {
		device = em.find(Device.class, device.getId());
		em.remove(device);
	}

	@SuppressWarnings("unchecked")
	public List<Device> getAllDevices() {
		return em.createNamedQuery("device.all").getResultList();
	}
}
