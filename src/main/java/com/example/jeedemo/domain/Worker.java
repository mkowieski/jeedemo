package com.example.jeedemo.domain;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;

@Entity
@NamedQueries({ 
	@NamedQuery(name = "worker.all", query = "Select w from Worker w")
})
public class Worker {

	private Long id;

	private String firstName = "";
	private String lastName = "";
	private String pesel = "";
	private Date registrationDate = new Date();
	private String street = "";
	private String postcode = "";
	private String city = "";
	
	private List<Specialization> specializations = new ArrayList<Specialization>();

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	
	@Size(min = 2, max = 20)
	public String getFirstName() {
		return firstName;
	}
	
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	
	@Size(min = 2, max = 20)
	public String getLastName() {
		return lastName;
	}
	
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
	@Size(min = 2, max = 11)
	public String getPesel() {
		return pesel;
	}
	
	public void setPesel(String pesel) {
		this.pesel = pesel;
	}
	
	@Size(min = 2, max = 20)
	public String getStreet() {
		return street;
	}
	
	public void setStreet(String street) {
		this.street = street;
	}
	
	@Size(min = 2, max = 20)
	public String getPostcode() {
		return postcode;
	}
	
	
	public void setPostcode(String postcode) {
		this.postcode = postcode;
	}
	
	@Size(min = 2, max = 20)
	public String getCity() {
		return city;
	}
	
	public void setCity(String city) {
		this.city = city;
	}
	
	@Temporal(TemporalType.DATE)
	public Date getRegistrationDate() {
		return registrationDate;
	}
	public void setRegistrationDate(Date registrationDate) {
		this.registrationDate = registrationDate;
	}
	
	// Be careful here, both with lazy and eager fetch type
		@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
		public List<Specialization> getSpecializations() {
			return specializations;
		}
		public void setSpecializations(List<Specialization> specializations) {
			this.specializations = specializations;
		}
}
