package com.example.jeedemo.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

@Entity
@NamedQueries({ 
@NamedQuery(name = "specialization.all", query = "Select s from Specialization s"),
@NamedQuery(name = "specialization.unspec", query = "Select s from Specialization s where s.spec = false")
})
public class Specialization {
	
	private Long id;
	private String name;
	private Boolean spec = false;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Boolean getSpec() {
		return spec;
	}
	public void setSpec(Boolean spec) {
		this.spec = spec;
	}
}
