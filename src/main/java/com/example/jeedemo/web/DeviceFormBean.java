package com.example.jeedemo.web;

import java.io.Serializable;

import javax.enterprise.context.SessionScoped;
import javax.faces.model.ListDataModel;
import javax.inject.Inject;
import javax.inject.Named;

import com.example.jeedemo.domain.Device;
import com.example.jeedemo.service.DeviceManager;

@SessionScoped
@Named("deviceBean")
public class DeviceFormBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private Device device = new Device();
	private ListDataModel<Device> devices = new ListDataModel<Device>();

	@Inject
	private DeviceManager dm;

	public Device getDevice() {
		return device;
	}
	public void setWorker(Device device) {
		this.device = device;
	}
	
	public ListDataModel<Device> getAllDevices() {
		devices.setWrappedData(dm.getAllDevices());
		return devices;
	}
	
	// Actions
	public String addDevice() {
		dm.addDevice(device);
		return "showDevices";
		//return null;
	}

	public String deleteDevice() {
		Device deviceToDelete = devices.getRowData();
		dm.deleteDevice(deviceToDelete);
		return null;
	}

}
