package com.example.jeedemo.web;

import java.io.Serializable;

import javax.enterprise.context.SessionScoped;
import javax.faces.model.ListDataModel;
import javax.inject.Inject;
import javax.inject.Named;

import com.example.jeedemo.domain.Specialization;
import com.example.jeedemo.service.SpecializationManager;

@SessionScoped
@Named("specializationBean")
public class SpecializationFormBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private Specialization specialization = new Specialization();
	private ListDataModel<Specialization> specializations = new ListDataModel<Specialization>();


	@Inject
	private SpecializationManager sm;

	public Specialization getSpecialization() {
		return specialization;
	}
	public void setSpecialization(Specialization specialization) {
		this.specialization = specialization;
	}
	
	public ListDataModel<Specialization> getAllSpecializations() {
		specializations.setWrappedData(sm.getAllSpecializations());
		return specializations;
	}
	
	// Actions
	public String addSpecialization() {
		sm.addSpecialization(specialization);
		return "showSpecializations";
		//return null;
	}

	public String deleteSpecialization() {
		Specialization specializationToDelete = specializations.getRowData();
		sm.deleteSpecialization(specializationToDelete);
		return null;
	}
}
