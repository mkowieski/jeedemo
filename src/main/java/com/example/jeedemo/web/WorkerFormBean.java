package com.example.jeedemo.web;

import java.io.Serializable;

import javax.enterprise.context.SessionScoped;
import javax.faces.model.ListDataModel;
import javax.inject.Inject;
import javax.inject.Named;

import com.example.jeedemo.domain.Specialization;
import com.example.jeedemo.domain.Worker;
import com.example.jeedemo.service.SpecManager;
import com.example.jeedemo.service.WorkerManager;

@SessionScoped
@Named("workerBean")
public class WorkerFormBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private Worker worker = new Worker();
	private ListDataModel<Worker> workers = new ListDataModel<Worker>();

	private Worker workerToShow = new Worker();
    private ListDataModel<Specialization> ownedSpecializations = new ListDataModel<Specialization>();

	@Inject
	private WorkerManager wm;
	
	@Inject
    private SpecManager sm;

	public Worker getWorker() {
		return worker;
	}
	public void setWorker(Worker worker) {
		this.worker = worker;
	}
	
	public ListDataModel<Worker> getAllWorkers() {
		workers.setWrappedData(wm.getAllWorkers());
		return workers;
	}
	
	public ListDataModel<Specialization> getOwnedSpecializations() {
        ownedSpecializations.setWrappedData(wm.getOwnedSpecializations(workerToShow));
        return ownedSpecializations;
}
	
	// Actions
	public String addWorker() {
		wm.addWorker(worker);
		return "showWorkers";
		//return null;
	}

	public String deleteWorker() {
		Worker workerToDelete = workers.getRowData();
		wm.deleteWorker(workerToDelete);
		return null;
	}
	
	public String showDetails() {
	        workerToShow = workers.getRowData();
	        return "details";
	}
	
	public String disposeSpecialization(){
	        Specialization specializationToDispose = ownedSpecializations.getRowData();
	        sm.disposeSpecialization(workerToShow, specializationToDispose);
	        return null;
	}

}
