package com.example.jeedemo.web;

import java.io.Serializable;
import java.util.List;

import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.example.jeedemo.domain.Specialization;
import com.example.jeedemo.domain.Worker;
import com.example.jeedemo.service.SpecManager;
import com.example.jeedemo.service.WorkerManager;

@SessionScoped
@Named("specBean")
public class SpecFormBean implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	private SpecManager sm;

	@Inject
	private WorkerManager wm;

	private Long specializationId;
	private Long workerId;
	
	public Long getSpecializationId() {
		return specializationId;
	}
	public void setSpecializationId(Long specializationId) {
		this.specializationId = specializationId;
	}
	public Long getWorkerId() {
		return workerId;
	}
	public void setWorkerId(Long workerId) {
		this.workerId = workerId;
	}

	public List<Specialization> getAvailableSpecializations() {
		return sm.getAvailableSpecializations();
	}

	public List<Worker> getAllWorkers() {
		return wm.getAllWorkers();
	}

	public String goSpec() {
		sm.goSpec(workerId, specializationId);
		return null;
	}
}
